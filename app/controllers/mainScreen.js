// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var toggleStatus = false;

init();

function init() {
    $.header.getView("menuButton").setBubbleParent(false);
    
    $.header.getView("menuButton").setText(Alloy.Globals.icon.menu);

    $.header.getView("menuButton").addEventListener("click", toggelSlider);
}

function fadeOutSlider() {
    if (toggleStatus) {
        toggelSlider();
    }
}

function toggelSlider(e) {
    
    //$.header.getView("menuButton").removeEventListener("click", toggelSlider);

    if (toggleStatus) {
        
        Ti.API.info('into true');

        $.fadeView.setTouchEnabled(true);
        $.fadeView.setBackgroundColor("#fff");
        
        $.slider.animate({
            width : "0%",
            duration : 100
        });
        
        $.fadeView.animate({
            left : "0%",
            duration : 400
        });
        
        $.fadeView.setLeft("0%");
        $.slider.setWidth("0%");

        // $.fadeView.animate({
            // backgroundColor : "#fff",
            // duration : 400
        // });

        //$.fadeView.animate(zoomout);

        //animation.fadeIn($.fadeView, 400);
        toggleStatus = false;

    } else {
        
        
        Ti.API.info('into false');

        $.fadeView.setTouchEnabled(false);
        $.fadeView.setBackgroundColor("#b3000000");

        $.fadeView.animate({
            left : "50%",
            duration : 400
        });
        
        $.slider.animate({
            width : "50%",
            duration : 500
        });
        
        // $.slider.setWidth(Ti.UI.SIZE);

        //$.fadeView.animate(zoom);
        
        
        $.fadeView.setLeft("50%");
        $.slider.setWidth("50%");

        //animation.fadeOut($.fadeView, 400);
        toggleStatus = true;
    }
    
   // $.header.getView("menuButton").addEventListener("click", toggelSlider);
    
}

function navigateToNext() {

    // var storeLocator = Alloy.createController("storeLocator").getView();
// 
    // $.mainScreen.animate(zoomout);
// 
//     
    // setTimeout(function() {
        // Alloy.Globals.currentWindow.remove($.mainScreen);
    // }, 700);
// 
    // Alloy.Globals.currentWindow.add(storeLocator);
    
    Alloy.Globals.navigateToNext("storeLocator");

}



function toggelSliderInOpen(){
    //toggleStatus=true;
    if(toggleStatus){
        
        toggelSlider();
    }
}

function navigateToTrackOrder(){
    Alloy.Globals.navigateToNext("pastOrders");
}
