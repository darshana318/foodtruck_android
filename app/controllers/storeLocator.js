var args = arguments[0] || {};

var Map = require('ti.map');

/*Attribute String*/

/*Attribute String*/

//var coord = Alloy.Globals.getCurrentLocation();

var mountainView = Map.createAnnotation({
    latitude : 33.74511, //coord.latitude,
    longitude : -84.38993, //coord.longitude,
    title : "Current Location",
    subtitle : 'Test',
    pincolor : Map.ANNOTATION_RED,
    myid : 1 // Custom property to uniquely identify this annotation.
});

var mountainView1 = Map.createAnnotation({
    latitude : 33.74611, //coord.latitude,
    longitude : -84.38993, //coord.longitude,
    title : "Current",
    subtitle : 'Test1',
    //pincolor : Map.ANNOTATION_RED,
    image : "/images/mapPing.png",
    myid : 0 // Custom property to uniquely identify this annotation.
});

var mountainView2 = Map.createAnnotation({
    latitude : 33.74811, //coord.latitude,
    longitude : -84.38993, //coord.longitude,
    title : "Current",
    subtitle : 'Test1',
    //pincolor : Map.ANNOTATION_RED,
    image : "/images/mapPing.png",
    myid : 0 // Custom property to uniquely identify this annotation.
});

// mountainView1.addEventListener("click",navigateToFoodMenu);
// mountainView2.addEventListener("click",navigateToFoodMenu);

var mapview = Map.createView({
    mapType : Map.NORMAL_TYPE,
    region : {
        latitude : 33.74511, //coord.latitude,
        longitude : -84.38993, //coord.longitude,
        latitudeDelta : 0.01,
        longitudeDelta : 0.01
    },
    animate : true,
    regionFit : true,
    userLocation : true,
    annotations : [mountainView, mountainView1, mountainView2]
});

mapview.addEventListener("click", navigateToFoodMenu);

$.map_container.add(mapview);

function enableSearch() {
    $.searchField.visible = true;
    $.screenLbl.visible = false;
    $.searchField.focus();
}

function toggelDetailList() {

    //Ti.API.info('toggel 1');

    if ($.store_list.getHeight() === "300dp") {
        $.store_list.animate({
            height : "40dp",
            duration : 500
        });
        setTimeout(function() {
            $.store_list.setHeight("40dp");
            $.filterLbl.hide();
            $.store_list_container.setVisible(false);
        }, 500);
        $.storeDetails.setBackgroundColor("#fff");
        $.storeDetails.setColor("#A4A4A4");
        $.storeDetails.setBorderColor("#A4A4A4");
    } else {
        $.store_list.animate({
            height : "300dp",
            duration : 500
        });

        setTimeout(function() {
            $.store_list.setHeight("300dp");
            $.filterLbl.show();
            $.store_list_container.setVisible(true);
        }, 500);
        $.storeDetails.setBackgroundColor(Alloy.Globals.labelTitleColor);
        $.storeDetails.setColor("#000");
        $.storeDetails.setBorderColor("transparent");
    }

    //Ti.API.info('toggel 2');

}

function openFilterList() {
    $.filterLbl.hide();
    $.filterContatiner.visible = true;
    $.filterListContainer.animate({
        height : "200dp",
        duration : 500
    });
    $.filterListContainer.setHeight("200dp");
}

function closeFilterList() {

    $.filterListContainer.animate({
        height : "0dp",
        duration : 500
    });
    setTimeout(function() {
        $.filterContatiner.setVisible(false);
        $.filterLbl.show();
    }, 500);
    $.filterListContainer.setHeight("0dp");
}

function goToBack() {
    Alloy.Globals.popWindowInNav();
    $.storeLocatorView.close();
}

function navigateToNextScreen() {
    // var foodmenu = Alloy.createController("foodtruckList").getView();
// 
    // $.storeLocatorView.animate(zoomout);
// 
    // $.storeLocatorView.hide();
    // Alloy.Globals.currentWindow.remove($.storeLocatorView);
    // setTimeout(function() {
        // Alloy.Globals.currentWindow.add(foodmenu);
    // }, 700);
    
     Alloy.Globals.navigateToNext("foodtruckList");

}

function navigateToFoodMenu(evt) {

    if (evt.clicksource == "title") {

        // var foodmenu = Alloy.createController("foodmenu").getView();
// 
        // $.storeLocatorView.animate(zoomout);
// 
        // $.storeLocatorView.hide();
        // Alloy.Globals.currentWindow.remove($.storeLocatorView);
        // setTimeout(function() {
            // Alloy.Globals.currentWindow.add(foodmenu);
        // }, 700);
        
        Alloy.Globals.navigateToNext("foodmenu");

    }

}

function destroyWindow(e) {
    $.storeLocatorView.removeAllChildren();
    $.destroy();
}
