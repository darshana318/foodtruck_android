// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

init();

function init() {
    var listData = [];

    listData.push({
        itemCount : {
            text : "1"
        }
    });

    $.listSection.appendItems(listData);

}

function navigateToNext() {
    
    
     Alloy.Globals.navigateToNext("chooseOption");
     
    /*
     var chooseOption = Alloy.createController("chooseOption").getView();

     $.yourCart.animate(zoomout);

     Alloy.Globals.currentWindow.remove($.yourCart);
     //setTimeout(function() {
     Alloy.Globals.currentWindow.add(chooseOption);
     //}, 700);
     */

}

function itemAddEvent(e) {
    try{
        
   

    var bindId = e.bindId;
    var index = e.itemIndex;
    itemIndex = index;
    var currentItem,
        listItem;
    if (bindId) {
        var bind = bindId;
        currentItem = e.section.items[index];
        listItem = currentItem[bind];
    }

    var itemCount = currentItem["itemCount"];

    var itemValue = parseInt(itemCount.text);

    if (bindId == "itemPlus") {
        // itemPlus
        itemValue++;
        itemCount.text = ((itemValue.length == 0 ? "0" + itemValue : itemValue));
        e.section.updateItemAt(itemIndex, currentItem);
    } else if (bindId == "itemMinus") {

        // itemMinus

        if (itemValue == 0) {
            itemValue = 0;
        } else {
            itemValue--;

        }

        itemCount.text = ((itemValue.length == 0 ? "0" + itemValue : itemValue));
        e.section.updateItemAt(itemIndex, currentItem);

    }
    
     }
    catch(exp){
        Ti.API.info('Expection---->' +exp.message);
    }
    
    

}
