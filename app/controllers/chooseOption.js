// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var selectOption = null;

init();

function init() {
    $.header.getView("overFlowMenuLbl").setText("Order Details");
    $.header.getView("overFlowMenuLbl").setVisible(true);

    $.header.getView("overFlowMenuLbl").setFont({
        fontSize : "12dp",
    });

    $.header.getView("overFlowMenuLbl").addEventListener("click", navigateToNext);

}

function chooseOption(e) {
    // Ti.API.info('e.id '+ e.id);
    // Ti.API.info('e.id '+ JSON.stringify(e.source.children));

    if (e.source.children[0].id == "rightNow") {
        $.rightNow.setColor("#000");
        $.in30min.setColor("#33000000");
        $.in45min.setColor("#33000000");
        selectOption = "rightNow";
    } else if (e.source.children[0].id == "in30min") {

        $.rightNow.setColor("#33000000");
        $.in30min.setColor("#000");
        $.in45min.setColor("#33000000");
        selectOption = "in30min";

    } else if (e.source.children[0].id == "in45min") {
        $.rightNow.setColor("#33000000");
        $.in30min.setColor("#33000000");
        $.in45min.setColor("#000");
        selectOption = "in45min";
    }

}

function navigateToNext(e) {

    if (selectOption != null) {
        Alloy.Globals.navigateToNext("orderSummary");
    } else {
        alert("Please Choose Pick-up Option");
    }

}


function submitOrderSummary(){
     Alloy.Globals.navigateToNext("signIn");
}
